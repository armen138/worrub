
import Burrow from "burrow";
import ProxySpartanRequest from "./ProxySpartanRequest.js";

class ProxyRouter {
    /**
     * Create static file Router
     * @param {object} config Reverse proxy configuration
     */
     constructor(config) {
        this.config = config;
    }
    /**
     * Try to resolve requested file
     * @param {Request} request The request that triggered this router
     * @param {string} route_path The normalized path scoped for this router
     * @returns {Response|false} Returns false if router could not match requested resource
     */
    resolve(request, route_path) {
        let promise = new Promise((resolve, reject) => {
            let proxyRequest = new ProxySpartanRequest(request, this.config);
            if(!proxyRequest.invalid) {
                proxyRequest.fetch().then(data => {
                    let response = new Burrow.Response().raw(data);
                    resolve(response);
                }).catch(err => {
                    let response = new Burrow.Response().fail(err, 5);
                    resolve(response);
                });
            } else {
                resolve(new Burrow.Response().fail("Proxy request denied", 5));
            }
        });
        return promise;
    }
}

export default ProxyRouter;