import net from "net";

class ProxySpartanRequest {
    constructor(request, config) {
        this.proxied_host = request.host;
        this.host = request.host;
        this.path = request.path;
        this.content_size = request.content_size;
        this.body = request.body;
        let host_config = config.filter(item => item.host == request.host);
        if (host_config.length > 0) {
            let host_port = host_config[0].pass_through.split(":");
            this.host = host_port[0];
            this.port = host_port[1];
        } else {
            this.invalid = true;
        }
    }
    fetch() {
        let promise = new Promise((resolve, reject) => {
            var client = net.createConnection({ host: this.host, port: this.port }, () => {
                let proxy_request = `${this.proxied_host} ${this.path} ${this.content_size}\r\n${this.body}\r\n`;
                client.write(proxy_request);
            });
            client.setTimeout(5000);
            client.setEncoding('utf8');
            client.on('data', data => resolve(data));
            client.on('timeout', () => reject("timeout"));
            client.on('error', err => reject(err.message));
        });
        return promise;
    }
}

export default ProxySpartanRequest;