import net from "net";

/** SNI Reverse Proxy class
 * @augments Server
*/
class SNI {
    routers = {};
    /**
     * Create an SNI Reverse Proxy server
     * @param {number} port The port to listen on
     * @param {number} max_connections How many connections to allow at once
     */
    constructor(port, max_connections, config) {
        this.port = port;
        this.config = config.filter(item => item.protocol.toLowerCase() === "sni");
        this.hosts = {};
        for(let item of this.config) {
            let pass_through = item.pass_through.split(":");
            this.hosts[item.host] = {
                "host": pass_through[0],
                "port": parseInt(pass_through[1] || "1965")
            }
        }
        this.create_server(max_connections);
    }
    create_server(max_connections) {
        this.server = net.createServer(socket => {
            let target = null;
            socket.on('error', err => {
                console.error("Socket error", err);
            });
            socket.on('data', (buffer) => {
                if (target) {
                    target.write(buffer);
                } else {
                    let hostname = this.decode_hello(buffer);
                    if (hostname && this.hosts[hostname]) {
                        target = net.createConnection({ host: this.hosts[hostname].host, port: this.hosts[hostname].port }, () => {
                            target.write(buffer);
                        });
                        target.setTimeout(5000);
                        target.on('data', data => socket.write(data));
                        target.on('timeout', () => socket.end());
                        target.on('error', err => socket.end());
                        target.on('end', () => socket.end());
                    } else {
                        // Don't know hostname, and we're not doing the TLS handshake either, so we can't return a PROXY REFUSED error as per gemini spec.
                        // Unceremoniously abort.
                        console.warn("Requesting hostname not configured, aborting");
                        socket.end();
                    }
                }
            });
        });
        this.server.maxConnections = max_connections;

    }
    bytes_to_number(bytes) {
        if(bytes.length === 0) return 0;
        return bytes.reduce((value, byte) => value * 256 + byte);
    }
    decode_hello(buffer) {
        try {
            let hello = Uint8Array.from(buffer);
            let index = 43; // skipping over fixed-length items
            let session_id_length = hello[index++];
            index += session_id_length;
            let cypher_suite_length = this.bytes_to_number(hello.slice(index, index + 2));
            // index += 2 + cypher_suite_length;
            // console.log("This should be 0x01:", hello[index++]);
            // console.log("This should be 0x00:", hello[index++]);
            index += 4 + cypher_suite_length;
            let extensions_size = this.bytes_to_number(hello.slice(index, index + 2)) + 1;
            index += 2;
            let extensions = hello.slice(index, extensions_size);
            let extension_index = 0;
            while (extension_index < extensions_size) {
                let extension_id = extensions.slice(extension_index, extension_index + 2);
                extension_index += 2;
                let extension_size = this.bytes_to_number(extensions.slice(extension_index, extension_index + 2));
                extension_index += 2;
                let extension_data = extensions.slice(extension_index, extension_index + extension_size);
                // console.log("Extension ID: ", extension_id);
                // console.log("Extension Size: ", extension_size);
                // console.log("Extension Data: ", extension_data);
                if (extension_id[0] == 0x0 && extension_id[1] == 0x0) {
                    // this is the SNI extension
                    let record_type = extension_data[2];
                    if (record_type !== 0x0) {
                        console.log("Record type is not DNS, this won't work.");
                    }
                    let hostname_size = this.bytes_to_number(extension_data.slice(3, 5));
                    // console.log("Hostname size is ", hostname_size);
                    let hostname = String.fromCharCode.apply(null, extension_data.slice(5, 5 + hostname_size));
                    // console.log("Hostname is", hostname);
                    // console.log(String.fromCharCode.apply(null, extension_data.slice(1)));
                    return hostname;
                }
                extension_index += extension_size;
            }
        } catch(e) {
            console.log("Failed to parse Client Hello");
        }
        return null;
    }
    /** Listen on this.port */
    listen() {
        this.server.listen(this.port);
    }
}

export default SNI;