import fs from "fs";
import Burrow from "burrow";
import ProxyRouter from "./worrub/ProxyRouter.js";
import SNI from "./worrub/SNI.js";

let spartan = new Burrow.Spartan(3000, 30);
let config = JSON.parse(fs.readFileSync("config.json"));
let sni = new SNI(1965, 30, config);

let proxy_router = new ProxyRouter(config);
spartan.mount("/", proxy_router);
spartan.listen();
sni.listen();
